/** \mainpage star-finder
 * \section intro_sec Introduction
 *
 * This is a star finder. It points to a fixed point in the clestial sky.
 * This software is not usable for planet or sun tracking. This the companion
 * software needed to build this project
 * http://www.instructables.com/id/Star-Finder/
 *
 * \section hw_sec Hardware required
 * - Arduino mini pro (harder to upload to than a nano, but easier for low pwr)
 * - DS3231, DS3232 RTC clock connected through I2C to pin A4 and A5
 * - 28BYJ-48 Stepper motor with ULN2003 driver board connected to pins 2-5
 *
 * Mount the stepper motor with it's axis tilted to Polaris. For my
 * lattitude (52), this means an ange between the ground plane and the motor
 * plane of 38 degrees. Rotate the base so that the projection of the motor
 * axis points North.
 *
 * The motor should be wired such that a positive step value rotates the
 * pointer clockwise when facing the motor (as the stars appear to rotate
 * counter-clockwise around Polaris and the motor is assumed to be under the
 * pointer). This code adheres to this requirement if the IN1-IN4 pins of the
 * ULN2003 driver board of the stepper motor are connected to D2-D5
 * respectively.
 *
 * Mount a pointer on the motor axis. The angle between the motor plane and
 * the pointer should be equal to the declination of the star, i.e. 19 degrees
 * for Arcturus, the brightest star in the northern hemisphere.
 *
 * The assumed stepper position is stored in the RAM of the RTC. The stepper
 * motor can be calibrated with a pushbutton on pin 9, which will change to
 * motor position without adjusting the NVRAM. Calibration is described in the
 * function calibration mode. The function set_south_local_time () has to be
 * modified for your particular location (longitude) and star.
 *
 * For operation in the southern hemisphere, the rotation of the motor should
 * be reversed, see the motor_pin defines. Also, the axle of the motor should
 * point to the southern galactic pole, which is a bit harder as there is no
 * clear identifyable star in that position.
 *
 * * \section tm_sec Sidereal time
 *
 * The software does not make any assumptions about the validity of the
 * (sidereal) time, nor the longitude, nor the 0-postion of the stepper motor
 * pointer. All three influence (only) the angle offset.
 */

// #define DEBUG_SERIAL
#ifdef DEBUG_SERIAL
#define SERIAL_BPS 9600
#endif

/**
 * \brief RTC, time and NVRAM stuff
 *
 *  All communication is done through the I2C bus, controlled using the
 *  Wire.h library. * Analog pin A4 (PC4) and A5 (PC5) are used as SDA and SCL
 *  respectively.
 *
 *  The RTC clock is a SyncProvider for the Time.h library implemented in
 *  DS3232RTC.h library. The time.h library uses an interval to determine if
 *  external sync is needed. However, this interval is measured using Timer0,
 *  which is disabled during powerdown. Therefor, the syncprovider should be set
 *  in loop () and before now() is called.
 *
 *  To ensure power usage is low, the VCC of the DS1307 is wired to a digital
 *  pin of the arduino. Whenever the RTC or NVRAM is used, call power_on() first
 *  and power_off when done. Failing to do so will hang the program.
 *
 *  We misuse the alarm registers of the DS3231 as NVRAM.
 *
 *  To set the RTC clock to UTC, use the following code for this hardware:
 *
 *  rtc_on (); *	 don't forget to power on
 *	 setTime (12, 39, 0, 13, 8, 2016); * // hour, min, sec, day, month, year
 *  RTC.set (now ());					// transfer to RTC
 *
 * see https://codebender.cc/sketch:360775
 *
 */

#include <Arduino.h>
#include <Wire.h>

#include "LowPower.h"
#include "Time.h"
#include "DS3232RTC.h"

#define rtc_power_pin 8 // powers the RTC chip
#define ALRM0 0x08		// Alarm register 0
#define ALRM1 0x0b		// Alarm register 1

uint8_t mhd[3] = {0, 0, 0}; // minute-hour-day
// maintained as a global to allow
// testing without a RTC present

/**
 * \brief Sidereal time stuff
 *
 * A sidereal day is defined as the time for the earth to make a complete
 * revolution with respect to the sky. This is not the same as a solar day
 * as that includes the motion of the earth around the sun, which adds one
 * complete revolution per year. Note that we make no attempt to calculate
 * the absolute siderial time. Since we need to calibrate the pointer, it
 * is sufficient to calculate the relative siderial time (seconds since an
 * arbitrary moment, and only within one sidereal day, being one full pointer
 * rotation)
 *
 * https://en.wikipedia.org/wiki/Sidereal_time states a mean sidereal day is
 * 23:56:4.0916 solar seconds. A solar day is 86400 solar seconds. Note that
 * a sidereal day is 86400 sidereal seconds!
 */

#define solar_ms_seconds_per_sidereal_day 86164091ull // https://online.unitconverterpro.com/conversion-tables/convert-group/factors.php?cat=time&unit=13&val=

#define solar_ms_seconds_per_step 42072ull
#define motor_pin 2		   // motor driver pin (plus next 3)
#define step_delay_fast 3  // normaly used, small to save battery life
#define step_delay_slow 25 // used for calibration
#define slop_steps 25
int step_delay_ms = step_delay_fast;

// pushbutton ******************************************************************
#define calibrate_pin 9 // pushbutton to ground

// battery and power ***********************************************************
#define beep_pin 7 // Piezo beeper
#define ADMUX_VCCWRT1V1 (_BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1))
boolean stop = false;

/**
 * \defgroup power_grp power management stuff
 *
 * These routines rely mostly on the LowPower.h library, with a few extra's
 * - if Serial is used, it is ended / restored at sleep / wakeup
 * - TwoWire (I2C) is disabled / enabled at sleep / wakeup
 * - TwoWire lines pullup resistors are disabled / enabled at sleep / wakeup
 * - Power to the RTC is removed / reinstalled at sleep / wakeup
 * The power routines are only called by sleep() (and once power_on in setup),
 * so there is no attempt to do smart power management by the (very short)
 * wakeup periods.
 *
 *
 */

/**
 * \ingroup power_grp
 * Power up peripherals
 */
void power_on()
{
	delay(1); // let hardware stabilize
#ifdef DEBUG_SERIAL
	Serial.begin(SERIAL_BPS); // reinitialize serial
#endif
	digitalWrite(A4, HIGH);					  // enable pull-up resistors
	digitalWrite(A5, HIGH);					  // from the I2C pins
	TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWEA); // re-initialize twowire.
	digitalWrite(rtc_power_pin, HIGH);		  // power up RTC NVRAM chip
	delay(1);								  // let hardware stabilize
}

/**
 * \ingroup power_grp
 * Power down peripherals
 */
void power_off()
{
	TWCR = 0;						  // disable twowire
	digitalWrite(rtc_power_pin, LOW); // power down RTC NVRAM chip
	digitalWrite(A4, LOW);			  // disable pull-up resistors
	digitalWrite(A5, LOW);			  // from the I2C pins
#ifdef DEBUG_SERIAL
	Serial.end(); // close Serial
#endif
	delay(1); // let hardware stabilize
}

/**
 * \ingroup power_grp
 * sleeps the processor for 40 seconds
 */
void sleep()
{
	power_off();
	for (int n = 10; n--;)
		LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
	power_on();
}

/**
 * \ingroup power_grp
 * Checks the battery voltage.
 */
boolean battery_check()
{
	// set reference to VCC and the measurement to the internal 1.1V reference
	if (ADMUX != ADMUX_VCCWRT1V1)
	{
		ADMUX = ADMUX_VCCWRT1V1;
		delayMicroseconds(350); // wait for Vref to settle
	}
	ADCSRA |= _BV(ADSC); // start conversion
	while (bit_is_set(ADCSRA, ADSC))
	{
	};					// wait for it to finish
	return (ADC < 351); // false when Vcc < 3.2 V
}

/**
 * \ingroup power_grp
 * Beep
 * @param ontime On in ms
 * @param offtime Delay off in ms
 */
void beep(int ontime, int offtime)
{
	if (ontime > 0)
	{
		pinMode(beep_pin, OUTPUT);
		digitalWrite(beep_pin, HIGH); // beeper on
		delay(ontime);
		digitalWrite(beep_pin, LOW); // beeper off
		pinMode(beep_pin, INPUT);
	}
	if (offtime > 0)
	{
		delay(offtime);
	}
}

/**
 * \defgroup rtc_grp RTC SRAM routines
 *
 * The very precise DS3231 has no additional NVRAM, so we are misusing the bcd
 * values in the alarm registers for that. Since the less precise DS1307 has
 * SRAM on the locations of those alarm registers, we can simply use the same
 * code, meant to stay within bounds of real (bcd) alarm values in the
 * respective register positions.
 *
 * https://github.com/JChristensen/DS3232RTC
 *
 * There are only two register sets (alarm register sets) are available so the
 * only valid values for adr are 0 and 1 (non zero). The allowed value range is
 * -1000 to 43639. Actual range used is -sop_steps to steps_per-revolution.
 *
 * The actual time routines are implemented in the DS3232RTC.h library and no
 * additional routenes are needed here.
 */

/**
 * \ingroup rtc_grp
 *
 * read integer from the SRAM. The value returned is between -1000 and 43639
 * @param adr Can be 0 or 1
 */
int rtcRead(int adr)
{
	uint16_t uvalue;
	// read from alarm registers 1 or 2
	RTC.readRTC(adr == 0 ? ALRM0 : ALRM1, mhd, 3);
	//    day in month               hour           min
	uvalue = (mhd[2] - 1) * (60 * 24) + (mhd[1] * 60) + mhd[0];
	// slight negative allowed for slop processing
	return uvalue - 1000;
}

/**
 * \ingroup rtc_grp
 *
 * write integer to the SRAM.
 * @param adr Can be 0 or 1
 * @param value should be between -1000 and 43639
 */
void rtcWrite(int adr, int value)
{
	uint16_t uvalue;
	// slight negative allowed for slop processing
	uvalue = value + 1000;
	mhd[0] = uvalue % 60;			   // min
	mhd[1] = (uvalue / 60) % 24;	   // hour
	mhd[2] = 1 + (uvalue / (60 * 24)); // day in month
	// write to alarm registers 1 or 2
	RTC.writeRTC(adr == 0 ? ALRM0 : ALRM1, mhd, 3);
}

/**
 * \defgroup calibration_grp calibration routines
 *
 * Force the system time to a known full south position  and therefor
 * highest azimuth of the star of your choice at your longitude. In this
 * example, Arcturus star, NL-Utrecht. We do NOT save it to the RTC. To
 * avoid messing with DST, use UTC, here as well as when setting the RTC
 * itself.

 * While this apprach requires this code to be modified for each longitude
 * or star it eases calibration so much it is worth the effort, also because
 * for a change in longitude a modification of the motor mount is needed,
 * and for a change of star the pointer mount, which I consider being much
 * harder.
 */

/**
 * \ingroup calibration_grp
 *
 * A function that forces the time to one that correspondents to an exact
 * South position of the choose star
 *
 * for arcturis from longitude 5.05
 * setTime(3, 57, 56, 20, 2, 2016); // hr, mn, se, dy, mo, yr
 *
 * for alcyone from longitude 5.25
 * setTime(17, 26, 22, 20, 2, 2016); // hr, mn, se, dy, mo, yr
 *
 * for alcyone from longitude 5.05
 * setTime(12, 46, 50, 2, 5, 2022); // hr, mn, se, dy, mo, yr

 * for castor from longitude 5.05
 * setTime(20, 33, 20, 2, 3, 2022); // hr, mn, se, dy, mo, yr

 * for capella from longiture 5.05
 * setTime(14, 16, 7, 2, 5, 2022); // hr, mn, se, dy, mo, yr
 */
void set_south_local_time()
{
	// for alcyone from longitude 5.05
	setTime(12, 46, 50, 2, 5, 2022); // hr, mn, se, dy, mo, yr
}

/**
 * \brief pointer / stepper motor stuff
 *
 * Stepper.h is useless as it keeps the coils engaged. The stepper motor will
 * NOT be driven through the shortest path as the gearing ratio is not an
 * integer value, see
 * https://grahamwideman.wikispaces.com/Motors-+28BYJ-48+Stepper+motor+notes
 *
 * However, the motors I obtained, were 2048 steps per rotation indeed. Make
 * sure you test the motor for at least 10 revolutions. The value for
 * solar_ms_seconds_per_step needs to be
 * 42072ull	for a true 2048 steps per revolution motor
 * 42281ull	for a 2037.886 steps per revolution motor
 * 167949ull	for a 513.0343 steps per revolution motor
 * The calculation is solar_ms_seconds_per_sidereal_day / steps_per_revolution
 *
 * We are using a 4 phase sequence. A full revolution corresponds to a full
 * sidereal day.
 */

/**
 * \ingroup motor_grp

 * energize in one of 4 step positions, or remove power
 * @param motor_pin_first Pin connected to the first motor coil
 * @param mode step to energize, one of four modes (0-3). Any other value
 *              de-energizes all motor coils
 */
void step_energize(int motor_pin_first, int mode)
{
	// mode ^= 0x3; // reverse

	switch (mode)
	{
	case 0:
		digitalWrite(motor_pin_first, HIGH);
		digitalWrite(motor_pin_first + 1, HIGH);
		digitalWrite(motor_pin_first + 2, LOW);
		digitalWrite(motor_pin_first + 3, LOW);
		break;
	case 1:
		digitalWrite(motor_pin_first, HIGH);
		digitalWrite(motor_pin_first + 1, LOW);
		digitalWrite(motor_pin_first + 2, LOW);
		digitalWrite(motor_pin_first + 3, HIGH);
		break;
	case 2:
		digitalWrite(motor_pin_first, LOW);
		digitalWrite(motor_pin_first + 1, LOW);
		digitalWrite(motor_pin_first + 2, HIGH);
		digitalWrite(motor_pin_first + 3, HIGH);
		break;
	case 3:
		digitalWrite(motor_pin_first, LOW);
		digitalWrite(motor_pin_first + 1, HIGH);
		digitalWrite(motor_pin_first + 2, HIGH);
		digitalWrite(motor_pin_first + 3, LOW);
		break;
	default: // de-energize
		digitalWrite(motor_pin_first, LOW);
		digitalWrite(motor_pin_first + 1, LOW);
		digitalWrite(motor_pin_first + 2, LOW);
		digitalWrite(motor_pin_first + 3, LOW);
		break;
	}
}

/**
 * \ingroup motor_grp
 *
 * Step to a motor position.
 *
 * No shortest path calculation is used. This is delibarate as the motor
 * gearing has no integer based ratio per revolution!! Normally, every movement
 * is just one step, so there is need, and it avoids cummulative errors.
 *
 * Every new position is saved to the NVRAM. This way, even if power is
 * interrupted during a movement, calibration is maintained as good as
 * reasonably possible (within one step).
 */
void step_to(int new_position, boolean calibrating)
{
	int step_position; // actual position
	int step_delta;	   // how to get there

	unsigned long time_ms;

	step_position = rtcRead(0);

#ifdef DEBUG_SERIAL
	Serial.print(now());
	Serial.print(" ");
	Serial.println(new_position);
#endif

	step_delta = new_position - step_position;

	// get the delta, process slop compensation if needed. Slop compensation
	// is enable on a counter clockwise rotation and when calibration is
	// false. slop_steps may be 0, which has the same effect as no slop
	// compensation at all
	if (step_delta < 0 && !calibrating)
	{
		step_delta -= slop_steps; // over rotate
	}

	// handle 'do nothing'. Saves a bit of battery life
	if (step_delta == 0)
		return;

	// sync to the next ms
	time_ms = millis() + 1;
	while ((long)(millis() < time_ms) > 0)
	{
	};
	// and energize at current position
	step_energize(motor_pin, step_position & 0x3);
	time_ms += step_delay_ms;

	// the motor is energized here, but delta is non zero so the stepper loop
	// will be entered and it will be de-energized at the end
	while (step_delta != 0)
	{
		while ((long)(millis() < time_ms) > 0)
		{
		};
		if (step_delta > 0) // clockwise
		{
			step_energize(motor_pin, ++step_position & 0x3);
			if (!calibrating)
				rtcWrite(0, step_position); // save
			step_delta--;
		}
		else if (step_delta < 0) // counter clockwise
		{
			step_energize(motor_pin, --step_position & 0x3);
			if (!calibrating)
				rtcWrite(0, step_position); // save
			if (++step_delta == 0 && !calibrating)
				step_delta += slop_steps;
		}

		// wait until
		time_ms += step_delay_ms;
	}
	while ((long)(millis() < time_ms) > 0)
	{
	};
	// de-energize motor
	step_energize(motor_pin, 99);
}

/**
 * Convert the given time value to a motor position. Note that the ABSOLUTE
 * position is irrelevant, as this same routine is used to calibrate the pointer
 * to a known position on a known longitude on a known date-time.
 *
 * @param t time, a 32 bit value in solar seconds since midnight 01-01-1970.
 * @returns position the motor should move to
 */
int time_to_position(time_t t)
{
	// Calculate milliseconds within the current sidereal day, then calculate steps.

	uint64_t ms;

	ms = t * 1000ull; // in ms units and 64 bits

	// Calculation introduces 1 ms cummulative drift per day, so 0.36 seconds
	// per year. As one step corresponds to roughly 40 seconds, drift by this
	// calculation is totally neglectable over the course of several decades
	ms = ms % solar_ms_seconds_per_sidereal_day; // ms in the sideral day

	// Calculation introduces a non cummulative error of less than 1/200th of a
	// step (0.2 seconds) and can be completely ignored.
	return (int)(ms / solar_ms_seconds_per_step);
}

/**
 * Calculate motor position from a time, and move the motir there
 * @param t time, a 32 bit value in solar seconds since midnight 01-01-1970.
 */
void set_pointer(time_t t)
{
	step_to(time_to_position(t), false); // rotate, not in calibration
}

/**
 * Calibration mode is activated by holding the calibration button during
 * boot. To resume normal operation a reboot is required.
 */
void calibration_mode()
{
	int step_position;
	int direction = 4; // 4 step to match energize state
	int count;		   // used to speed up after 80 steps

	beep(100, 0);

	set_south_local_time(); // set time/date to known south

	// Move the pointer position 0 (rollover position) to hint absolute position
	step_to(0, false);
	delay(250);

	// Now move to the assumed south position
	step_position = time_to_position(now());
	step_to(step_position, false);

	while (!digitalRead(calibrate_pin))
	{
	} // wait until the button is released

	while (true)
	{
		while (digitalRead(calibrate_pin))
		{
		} // wait until the button is pressed

		count = 0;
		step_delay_ms = step_delay_slow;
		while (!digitalRead(calibrate_pin))
		{
			// while button is pressed rotate motor, do not save position
			step_to(step_position + direction, true);
			// speed up if pressed for a longer time
			if (count++ == 20)
				step_delay_ms = step_delay_fast;
		}
		direction = -direction; // switch direction
	}
}

/**
 * Initialize all subsystems
 */
void setup()
{
	// tell them I am alive
	beep(100, 200);
	for (int n = 0; n <= 3; n++)
	{
		// enable all motor pins
		pinMode(motor_pin + n, OUTPUT);
	}

	pinMode(rtc_power_pin, OUTPUT); // power up RTC
	power_on();						// power up peripherals inc Serial

	pinMode(calibrate_pin, INPUT);
	digitalWrite(calibrate_pin, HIGH); // enable pullup calibrate
	if (!digitalRead(calibrate_pin))   // calibrate button pushed?
	{
		calibration_mode(); // jump into calibrate mode
	}
	digitalWrite(calibrate_pin, LOW); // disable pullup calibrate

	// show south position to hint proper calibration
	set_south_local_time();
	step_to(time_to_position(now()), false);
	delay(500);
}

/**
 * The system loop. Checks the battery, fetches the time and moves the pointet
 * to the corresponding position, after which it will sleep for 40 seconds.
 */
void loop()
{
	if (stop) // a stop is final
	{
		beep(50, 0); // warn user
	}
	else if (battery_check()) // if there is enough power
	{
		// Set the plugin function to get the time from the RTC. As the processor
		// is powered down, including Timer0, the SyncProvider needs to be set
		// in the loop() as it forces a resync of the the Time.
		setSyncProvider(RTC.get);
		// adjust pointer
		step_to(time_to_position(now()), false);
	}
	else
	{
		beep(50, 0);
		stop = true; // set the stop state
	}
	sleep(); // low power state
}
